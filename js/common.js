$(document).ready(function() {
     
     /*Range price*/
     
    $( "#slider-range" ).slider({
      range: true,
      min: 1000000,
      max: 10000000,
      values: [ 3000000, 7000000 ],
      slide: function( event, ui ) {
        $( "#min" ).val( "Rp " + convertToRupiah(ui.values[ 0 ]) );
        $( "#max" ).val( "Rp " + convertToRupiah(ui.values[ 1 ]) );
      }
    });
    $( "#min" ).val( "Rp " + convertToRupiah($( "#slider-range" ).slider( "values", 0 )) );
    $( "#max" ).val( "Rp " + convertToRupiah($( "#slider-range" ).slider( "values", 1 )) );
           
           
    /*Front End Slider Config*/
    $("#slide").webwidget_slideshow_dot({
        slideshow_time_interval: '5000',
        slideshow_window_width: '1024',
        slideshow_window_height: '380',
        directory: 'img/'
    });
    /*End of Front End Slider Config*/


/*Tabs Container Config*/
    $('#tab-container').easytabs();
    $('#package-tab-container').easytabs();
/*End Of Tabs Container Config*/


   /*Gallery Detail Package*/
        $(".galImg").click(function() {
                var image = $(this).attr("rel");
                var title = $(this).attr("title");
                $('#feature').fadeOut('slow');
                $('#feature').html('<img src="' + image + '" width="657"/><br/><b>'+ title +'</b>');
                $('#feature').fadeIn('slow');
        });   
   /*End of gallery Detail Package*/


/*Date Picker Config*/
    $('#dp1').datepicker({
				format: 'mm-dd-yyyy'
			});
                    
 var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');          
        
 /*End of Date Picker Config*/      
       
    
/*Dokter Slider*/    
$(".home-carousel").jCarouselLite({
            btnNext: ".next",
            btnPrev: ".prev",
            auto: 2000,
                    speed: 2000
        });    
        
});
/*End Of*/

function convertToRupiah(angka)
{
	var rupiah = '';		
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return rupiah.split('',rupiah.length-1).reverse().join('');
}